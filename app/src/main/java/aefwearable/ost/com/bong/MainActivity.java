package aefwearable.ost.com.bong;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanFilter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ost.wearable.BongDevice;
import com.ost.wearable.OperationType;
import com.ost.wearable.WearableListener;
import com.ost.wearable.WearbleDevice;

import java.util.ArrayList;
import java.util.List;

public class MainActivity  extends ListActivity implements WearableListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;


    private BluetoothManager bluetoothManager;
    ///////////////////////////////////
    private List<ScanFilter> filters;
    private List<String> tt;
    ////////////////////////////////////
    public WearbleDevice bongDevice;
    ///////////////////////////////////
    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
//    private static final long SCAN_PERIOD = 10000;
    @Override
    protected void onDestroy() {
        super.onDestroy();
//        bongDevice.removeWearableListenr(MainActivity.this);
        unbindService(mServiceConnection);
        bongDevice = null;
    }


    @Override
    public void onConnected(boolean stateConnected,int stateActivity) {

    }

    @Override
    public void onDisconnected(boolean stateConnected) {

    }

    @Override
    public void onError(String error) {
        Log.e(TAG,error);
    }

    @Override
    public void onReceivedData(OperationType type, String data) {

    }

    @Override
    public void addListener(WearableListener listener) {

    }

    @Override
    public void onDiscoverDevices(BluetoothDevice devices) {
//            Log.d(TAG,"In onDiscoverDevices");
        if(!devices.equals(null)) {

                mLeDeviceListAdapter.addDevice(devices);
                mLeDeviceListAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        bongDevice = BongDevice.getInstance();
//        bongDevice.addWearableListener(this);


        mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "w,j", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
         bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this,"w,j", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }



        final Intent gattServiceIntent = new Intent(this, BongDevice.class);
        getApplicationContext().startService(gattServiceIntent);

        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);


    }



    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            bongDevice = ((BongDevice.LocalBinder) service).getService();
            // Automatically connects to the device upon successful start-up initialization.

            bongDevice.addWearableListener(MainActivity.this);
            bongDevice.setBluetoothInitialize(mBluetoothAdapter);
//            bongDevice.connect(mDeviceAddress);
            bongDevice.discover(true);
            if (!bongDevice.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bongDevice = null;
        }
    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                //scanLeDevice(true);
                break;
            case R.id.menu_stop:
               // scanLeDevice(false);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
        bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        /////////////////////////////////////
        filters = new ArrayList<ScanFilter>();
        // Initializes list view adapter.
//        bongDevice=new BongDevice();
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);
//bongDevice.setBluetoothManager(bluetoothManager);
//        bongDevice.setBluetoothAdapter(mBluetoothAdapter);

//        bongDevice.discover(true);
//        scanLeDevice(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        bongDevice.discover(mBluetoothAdapter,false);
//        scanLeDevice(false);
//        mLeDeviceListAdapter.clear();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
        if (device == null) return;
        final Intent intent = new Intent(this, DeviceControlActivity.class);
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_NAME,device.getName());
        intent.putExtra(DeviceControlActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());


//        if (mScanning) {
//            bongDevice.discover(false);
//            mScanning = false;
//        }

       // bongDevice.removeWearableListenr(this);
        startActivity(intent);
    }
//
//    private void scanLeDevice(final boolean enable) {
//        if (enable) {
//            // Stops scanning after a pre-defined scan period.
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    mScanning = false;
//                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
//                    invalidateOptionsMenu();
//                }
//            }, SCAN_PERIOD);
//
//            mScanning = true;
//            mBluetoothAdapter.startLeScan(mLeScanCallback);
//        } else {
//            mScanning = false;
//            mBluetoothAdapter.stopLeScan(mLeScanCallback);
//        }
//        invalidateOptionsMenu();
//    }

    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator = MainActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                ////////////////////////////////////
                viewHolder.aef1=(TextView) view.findViewById(R.id.textView);
                viewHolder.aef2=(TextView) view.findViewById(R.id.textView2);

                tt=new ArrayList<String>();
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mLeDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0){
                viewHolder.deviceName.setText(deviceName);
                /////////////////////////////////////////
                viewHolder.saef=deviceName;
                viewHolder.aef1.setText( device.getName()+" >>"+device.getAddress());


            }
            else {
                viewHolder.deviceName.setText("ไม่รู้จัก");
              //  viewHolder.saef+=deviceName;

                viewHolder.aef1.setText("ไม่รู้จัก >>"+device.getAddress());

            }
           // tt.add(i,device.getAddress());
            viewHolder.saef=tt.toString();
            viewHolder.deviceAddress.setText(device.getAddress());

            viewHolder.aef2.setText(viewHolder.saef);


            return view;
        }
    }

    // Device scan callback.
//    private BluetoothAdapter.LeScanCallback mLeScanCallback =
//            new BluetoothAdapter.LeScanCallback() {
//
//                @Override
//                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mLeDeviceListAdapter.addDevice(device);
//                            mLeDeviceListAdapter.notifyDataSetChanged();
//                        }
//                    });
//                }
//            };

    static class ViewHolder {
        TextView aef1;
        TextView aef2;
        TextView deviceName;
        TextView deviceAddress;
        String saef;
    }
}