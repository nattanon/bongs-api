/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aefwearable.ost.com.bong;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class SampleGattAttributes {
    private static HashMap<String, String> attributes = new HashMap();
    public static String HEART_RATE_MEASUREMENT =  "6e400001-b5a3-f393-e0a9-e50e24dcca1e";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "6e400002-b5a3-f393-e0a9-e50e24dcca1e";
   public static String FAN_CONTROL_SERVICE_UUID     = "86c86302-fc10-1399-df43-fceb24618252";
   public static String FAN_OPERATING_STATE = "705d627b-53e7-eda2-2649-5ecbd0bbfb85";

    public static String TOTALSTRINGSTEPS="";
    static {
        // Sample Services.6e400003-b5a3-f
        attributes.put("6e400002-b5a3-f393-e0a9-e50e24dcca1e", "TX Characteristic(ส่ง)");
        attributes.put(CLIENT_CHARACTERISTIC_CONFIG, "RX Characteristic(รับ)");
        attributes.put("6e400001-b5a3-f393-e0a9-e50e24dcca1e", "device(bong2s) Service");
        attributes.put("00002a00-0000-1000-8000-00805f9b34fb", "ชื่ออุปกรณ์");
        attributes.put("00002a04-0000-1000-8000-00805f9b34fb", "Peripheral");
        attributes.put("00002a01-0000-1000-8000-00805f9b34fb", "Appearance");
        attributes.put("00001800-0000-1000-8000-00805f9b34fb", "Generic Access");
        attributes.put("00001801-0000-1000-8000-00805f9b34fb", "Generic Attribute");



        // Sample Characteristics.
        attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");
        attributes.put("abcbe138-a00c-6b8e-7d44-4b63a80170c3", "DarkBlue Company UUID Characteristic");
        attributes.put("26b8a2dc-544d-008e-c343-5d8fac910c6e", "DarkBlue Major ID Characteristic");
        attributes.put("0acea172-2a76-69a7-4e49-302ed371f6a8", "DarkBlue Minor ID Characteristic");
        attributes.put("e205929f-e016-ecbc-024b-62f0658fdacd", "DarkBlue Measured Power Characteristic");
        attributes.put("519fc39c-9a18-82b2-ea43-6a101ab1a8f9", "Cooking Step Characteristic");
        attributes.put("52812a3d-247d-77b9-6740-75748c4621c5", "Cooking Step Characteristic");
        attributes.put("f47e47ab-b25f-3a83-7b47-1f230b2b1a61", "Cooking Step Characteristic");
     attributes.put(FAN_OPERATING_STATE, "Fan Operating State Characteristic");

    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
