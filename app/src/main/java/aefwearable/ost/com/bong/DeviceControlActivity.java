
package aefwearable.ost.com.bong;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.ost.wearable.BongDevice;
import com.ost.wearable.BongState;
import com.ost.wearable.OperationType;
import com.ost.wearable.UIDevice;
import com.ost.wearable.WearableListener;
import com.ost.wearable.WearbleDevice;

import static com.ost.wearable.BongState.STATE_CONNECTING;
import static com.ost.wearable.OperationType.*;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity implements WearableListener {
    public WearbleDevice bongDevice;
    BluetoothManager mBluetoothManager;
    BluetoothAdapter mBluetoothAdapter;

    ////////////////////////////////////////////////
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";


    BluetoothGatt mBluetoothGatt;
    private TextView mConnectionState;
    private TextView mDataField, mTType, mTData;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    // private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);
//        bongDevice = new BongDevice();
//        bongDevice.addWearableListener(this);
        mBluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBluetoothManager != null) {
//            bongDevice.setBluetoothManager(mBluetoothManager);
        }

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
//        mConnectionState.setText("เพิ่งเริ่มสร้าง");
        mDataField = (TextView) findViewById(R.id.data_value);

//        getActionBar().setTitle(mDeviceName);
//        getActionBar().setDisplayHomeAsUpEnabled(true);

        final Intent gattServiceIntent = new Intent(this, BongDevice.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        Button StartBtn = (Button) findViewById(R.id.button4);
        StartBtn.setText("Connect");
        StartBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                bongDevice.connect(mDeviceAddress);
                //
            }
        });

        Button findBtn = (Button) findViewById(R.id.button);
        findBtn.setText("Disconnect");
        findBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                bongDevice.disconnect();
            }
        });

        Button OpenBtn = (Button) findViewById(R.id.button2);
        OpenBtn.setText("Result_Steps.");
        OpenBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                bongDevice.setCharacteristicNotification(true);
//                 byte[] value  = {0x20,0x00,0x00,0x00,0x13,0x10,0x07,0x0C,0x10,0x00,0x10,0x07,0x0C,0x10,0x10};
                Date dt = new Date();
                Date d = new Date();
                Calendar c = Calendar.getInstance();
                d = new Date("yyyy-MM-dd HH:mm:ss");
                bongDevice.getSteps(d,dt);
            }
        });
        Button resultBtn = (Button) findViewById(R.id.button3);
        resultBtn.setText("Result_HR.");
        resultBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                bongDevice.getCurrentHeartRate();
                ////////////////////////////////
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            bongDevice = ((BongDevice.LocalBinder) service).getService();
            if (!bongDevice.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            bongDevice.addWearableListener(DeviceControlActivity.this);
            bongDevice.connect(mDeviceAddress);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bongDevice = null;
        }
    };

    //    // Handles various events fired by the Service.
//    // ACTION_GATT_CONNECTED: connected to a GATT server.
//    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
//    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
//    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
//    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, context.toString());
            final String action = intent.getAction();
            if (BongDevice.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();

                displayData(intent.getStringExtra(BongDevice.EXTRA_DATA));
            } else if (BongDevice.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BongDevice.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(bongDevice.getSupportedGattServices());
            } else if (BongDevice.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BongDevice.EXTRA_DATA));
            }
        }
    };
    //
//    // If a given GATT characteristic is selected, check for supported features.  This sample
//    // demonstrates 'Read' and 'Notify' features.  See
//    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
//    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                bongDevice.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            bongDevice.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                bongDevice.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            //characteristic.setValue(bytes);
                            //characteristic.setValue("testing");
                            //characteristic.setWriteType(BluetoothGattCharacteristic.PERMISSION_WRITE);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 16) {
                            mNotifyCharacteristic = characteristic;
                            bongDevice.setCharacteristicNotification(
                                    characteristic, true);
                        }

                    }
                    return false;
                }
            };

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }


    @Override
    protected void onResume() {
        super.onResume();

        /////การสร้าง Register
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

        mBluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (bongDevice != null) {
            bongDevice.connect(mBluetoothAdapter, mDeviceAddress);
            try {

//            bongDevice.connect(mBluetoothManager,mDeviceAddress);
//            bongDevice.connect(mDeviceAddress);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        /////การลบ Register
        unregisterReceiver(mGattUpdateReceiver);
    }

    //
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        bongDevice = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_connect:
                bongDevice.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                bongDevice.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //
    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    //
    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
            //  Log.i(TAG,"ข้อมูล Activity. :: "+data.toString());
        }
    }

    //
//    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
//    // In this sample, we populate the data structure that is bound to the ExpandableListView
//    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2},
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[]{LIST_NAME, LIST_UUID},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    //
//
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BongDevice.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BongDevice.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BongDevice.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BongDevice.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    ///////////////////////////////////////////////////////////////////
    @Override
    public void onConnected(boolean stateConnected, int stateActivity) {

//        if (stateActivity==BongState.STATE_CONNECTED.getTypeValue()){
//            mConnectionState.setText("Connected");
//        }else if (stateActivity==BongState.STATE_CONNECTING.getTypeValue()){
//            mConnectionState.setText("Connecting");
//        }else if (stateActivity==BongState.STATE_DISCONNECTED.getTypeValue()){
//            mConnectionState.setText("Disconnected");
//        }else{
//            mConnectionState.setText("Errer");
//            Log.e(TAG,"Value of onConnected ERRER!!");
//        }


    }

    @Override
    public void onDisconnected(boolean stateDisconnected) {

    }

    @Override
    public void onError(String error) {
        Log.e(TAG, error);
    }

    @Override
    public void onReceivedData(OperationType type, String data) {
//        mTType=(TextView) findViewById(R.id.textViewName);
//        mTData=(TextView) findViewById(R.id.textViewValue);
//        if (type!=null)
//        switch (type) {
//            case HEARTRATE:
//                mTType.setText("HeatRate");
//                break;
//            case BATTERY:
//                mTType.setText("BatTery");
//                break;
//            case STEPS:
//                mTType.setText("Steps");
//                break;
//        }
//        if (data!=null)
//        mTData.setText(data.toString());
        Log.d(TAG,"onReceivedData :: "+data+type.toString());
    }

    @Override
    public void addListener(WearableListener listener) {

    }

    @Override
    public void onDiscoverDevices(BluetoothDevice devices) {

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DeviceControl Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://aefwearable.ost.com.bong/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DeviceControl Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://aefwearable.ost.com.bong/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
