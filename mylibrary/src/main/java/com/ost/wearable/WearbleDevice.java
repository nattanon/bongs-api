package com.ost.wearable;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.provider.ContactsContract;

import java.util.Date;
import java.util.List;

/**
 * Created by Nattanon-PC on 14/7/2016.
 */
public interface WearbleDevice {
    void addWearableListener(WearableListener listener);

    void removeWearableListenr(WearableListener listener);

    void discover(BluetoothManager mBluetoothManager,boolean enable);
    void discover(BluetoothAdapter mBluetoothAdapter,boolean enable);
    void discover(boolean enable);



    void connect(BluetoothManager mBluetoothManager,String address);
    void connect(BluetoothAdapter mBluetoothAdapter,String address);
    void connect(String address);

    void disconnect();

    void setBluetoothInitialize(BluetoothManager mBluetoothManager);
    void setBluetoothInitialize(BluetoothAdapter mBluetoothAdapter);

    void getSteps();
    void getSteps(Date startTime, Date endTime);

    void getCurrentHeartRate();
    void getHistoryHeartRate(Date startTime, Date endTime);

    //รับค่า List BluetoothGattService ที่มีอยู่ใน Ojectที่สร้าง
    List<BluetoothGattService> getSupportedGattServices();

    boolean initialize();

        //method เดิมเรียกโดยการส่ง UI characteristic
    void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled);
    void setCharacteristicNotification(boolean enabled);
    void writeRemoteCharacteristic(byte[] value);
    void writeRemoteCharacteristic(BluetoothGattCharacteristic characteristic);
    void writeRemoteCharacteristic(BluetoothGattCharacteristic characteristic,byte[] value);


    void readCharacteristic(BluetoothGattCharacteristic characteristic);



}
