package com.ost.wearable;

/**
 * Created by Nattanon-PC on 15/7/2016.
 */
public enum UIDevice {
    SERVICE("6e400001-b5a3-f393-e0a9-e50e24dcca1e"),
    TX_CHARACTERISTIC("6e400002-b5a3-f393-e0a9-e50e24dcca1e"),
    RX_CHARACTERISTIC("6e400003-b5a3-f393-e0a9-e50e24dcca1e"),
    CLIENT_CHARACTERISTIC_CONFIG("00002902-0000-1000-8000-00805f9b34fb");

    private String typeValue;

    UIDevice(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return this.typeValue;
    }
}

