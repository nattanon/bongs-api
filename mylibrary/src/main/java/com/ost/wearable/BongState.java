package com.ost.wearable;

/**
 * Created by Nattanon-PC on 20/7/2016.
 */
public enum BongState {
    SCAN_PERIOD (10000),
    STATE_DISCONNECTED(0),
    STATE_CONNECTING (1),
    STATE_CONNECTED (2);

    private int typeValue;

    BongState(int typeValue) {
        this.typeValue = typeValue;
    }

    public int getTypeValue() {
        return this.typeValue;
    }
}

