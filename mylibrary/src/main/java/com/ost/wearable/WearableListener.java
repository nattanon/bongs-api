package com.ost.wearable;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;

import java.util.List;


/**
 * Created by Nattanon-PC on 14/7/2016.
 */
public interface WearableListener{

    void onConnected(boolean stateConnected,int stateActivity);

    void onDisconnected(boolean stateDisconnected);

    void onError(String error);

    void onReceivedData(OperationType type, String data);

    void addListener(WearableListener listener);

    void onDiscoverDevices(BluetoothDevice devices);
}
