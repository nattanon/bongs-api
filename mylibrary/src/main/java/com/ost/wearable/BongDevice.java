package com.ost.wearable;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


public class BongDevice extends Service implements WearbleDevice{
    private final static String TAG =BongDevice.class.getSimpleName();

    ////////////////////discover/////////////////////////
    private static DeviceScan scan;
    private static final int SCAN_PERIOD =BongState.SCAN_PERIOD.getTypeValue();
    ///////////////////////bluetoothService//////////////////////////
//    private static  BluetoothLeService mBluetoothLeService;
    private static BluetoothManager mBluetoothManager;
    private static BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothGattServer mBluetoothGattServer;
    private int mConnectionState = BongState.STATE_DISCONNECTED.getTypeValue();
    public final static String ACTION_GATT_CONNECTED =
            "com.ost.wearable.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.ost.wearable.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.ost.wearable.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.ost.wearable.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.ost.wearable.EXTRA_DATA";
    ///////////////////////////////////////////////////////////////


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "service is started");
        return Service.START_STICKY;
    }

    public static List<WearableListener> bListener;
    public static final BongDevice device = new BongDevice();
    public BongDevice() {
        bListener = new ArrayList<>();
    }

//    public static BongDevice getInstance() {

//        return device;
//    }

    private void setMessageError(String Message){
        for (WearableListener listener : bListener) {
            listener.onError(Message);
        }
    }

    private void setStateConnecting(boolean stateConnected,int stateActivity){
        for (WearableListener listener : bListener) {
            listener.onConnected(stateConnected,stateActivity);
        }
    }

    private void setOnReceivedData(OperationType type, String data){
        for (WearableListener listener : bListener) {
            listener.onReceivedData(type,data);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void discover(BluetoothAdapter mBluetoothAdapter,boolean enable){

        try {
            setBluetoothInitialize(mBluetoothAdapter);
            scan=new DeviceScan();
            scan.IsScan(enable);
        } catch (Exception e) {

            setMessageError(e.toString());
            Log.e(TAG, e.toString());
        }

   }
    @Override
    public void discover(BluetoothManager mBluetoothManager,boolean enable){

        try {
            setBluetoothInitialize(mBluetoothManager);
            scan=new DeviceScan();
            scan.IsScan(enable);
        } catch (Exception e) {

            setMessageError(e.toString());
            Log.e(TAG, e.toString());
        }

    }
    @Override
    public void discover (boolean enable){
        try {
            scan=new DeviceScan();
            scan.IsScan(enable);
        } catch (Exception e) {

            setMessageError(e.toString());
            Log.e(TAG, e.toString());
        }

    }

    @Override
    public void setBluetoothInitialize(BluetoothManager mBluetoothManager) {
//        if (this.mBluetoothManager==null)
        this.mBluetoothManager=mBluetoothManager;
        this.mBluetoothAdapter=mBluetoothManager.getAdapter();
    }
    @Override
    public void setBluetoothInitialize(BluetoothAdapter mBluetoothAdapter) {
//        if (this.mBluetoothAdapter==null)
        this.mBluetoothAdapter=mBluetoothAdapter;
    }

    @Override
    public void addWearableListener(WearableListener listener) {

        Log.d(TAG, "add wearalbe listener");
        bListener.add(listener);
    }
    @Override
    public void removeWearableListenr(WearableListener listener) {
        bListener.remove(listener);
    }

    @Override
    public void connect(BluetoothManager mBluetoothManager,String address) {
        setBluetoothInitialize(mBluetoothManager);
        connectInlibrary(address);

    }
    @Override
    public void connect(BluetoothAdapter mBluetoothAdapter,String address) {
        setBluetoothInitialize(mBluetoothAdapter);
        connectInlibrary(address);
    }
    @Override
    public void connect(String address){
        connectInlibrary(address);
    }

    @Override
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        }
        mBluetoothGatt.disconnect();
        Log.i(TAG,"Disconnect Success ::"+mBluetoothDeviceAddress);

        for (WearableListener listener : bListener) {
            listener.onDisconnected(true);
        }
    }

    @Override
    public void getSteps(){
        byte[] value  = {0x20,0x00,0x00,0x00,0x13,0x10,0x07,0x0C,0x10,0x00,0x10,0x07,0x0C,0x10,0x10};
        if (mBluetoothGatt == null) {
            setMessageError("lost connection");
            Log.e(TAG, "lost connection");
        }
        BluetoothGattService Service = mBluetoothGatt.getService(UUID.fromString(UIDevice.SERVICE.getTypeValue()));
        if (Service == null) {
            setMessageError("service not found!");
            Log.e(TAG, "service not found!");
        }
        BluetoothGattCharacteristic charac = Service
                .getCharacteristic(UUID.fromString(UIDevice.TX_CHARACTERISTIC.getTypeValue()));
        if (charac == null) {
            setMessageError("char not found!");
            Log.e(TAG, "char not found!");
        }
        charac.setValue(value);
        writeRemoteCharacteristic(value);
        setCharacteristicNotification(true);
    }


    @Override
    public void getSteps(Date startTime, Date endTime) {


    }

    @Override
    public void getCurrentHeartRate(){
//        setCharacteristicNotification(true);
//        byte[] value  = {0x26,0x00,0x00,0x00,0x52};
        byte [] value=OperationType.HEARTRATE.getTypeValue();
        //check mBluetoothGatt is available
        if (mBluetoothGatt == null){
            setMessageError("lost connection");
            Log.e(TAG, "lost connection");
        }
        BluetoothGattService Service = mBluetoothGatt.getService(UUID.fromString(UIDevice.SERVICE.getTypeValue()));
        if (Service == null) {
            setMessageError("service not found!");
            Log.e(TAG, "service not found!");
        }
        BluetoothGattCharacteristic charac = Service
                .getCharacteristic(UUID.fromString(UIDevice.TX_CHARACTERISTIC.getTypeValue()));
        if (charac == null) {
            setMessageError("char not found!");
            Log.e(TAG, "char not found!");
        }
        writeRemoteCharacteristic(charac,value);
        setCharacteristicNotification(true);
//        setCharacteristicNotification(false);
    }

    @Override
    public void getHistoryHeartRate(Date startTime, Date endTime) {
        try{
        Integer.valueOf(startTime.getYear());
        startTime.getMonth();
        startTime.getDate();
        startTime.getHours();
        startTime.getMinutes();

        }catch (Exception e) {
            setMessageError(e.toString());
        }

//        Integer.toHexString();

    }

    public class LocalBinder extends Binder {
        public BongDevice getService() {
            return BongDevice.this;
        }
    }

    private class DeviceScan extends Activity {

        private boolean mScanning;
        private Handler mHandler;

        private void IsScan(boolean enable){
            ///เริ่มค้นหา/////
            mHandler = new Handler();
            mScanning = true;
            if (enable) {
                // Stops scanning after a pre-defined scan period.
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mScanning = false;
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);

                    }
                }, SCAN_PERIOD);

                mScanning = true;
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                mScanning = false;
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }

        private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback()
        {
            @Override
            public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!device.equals(null)) {
                            for (WearableListener listener : bListener) {
                                listener.onDiscoverDevices(device);
                            }
                        }

                    }
                });
            }
        };
    }

    public boolean connectInlibrary(String address) {
//        if (mBluetoothAdapter == null || address == null) {
//            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
//            setStateConnecting(false,mConnectionState);
//            return false;
//        }
//
//        // Previously connected device.  Try to reconnect.
//        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
//                && mBluetoothGatt != null) {
//            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
//            if (mBluetoothGatt.connect()) {
//                mConnectionState = STATE_CONNECTING;
//                Log.d(TAG,"Connect Return true");
//                setStateConnecting(true,mConnectionState);
//                return true;
//            } else {
//                setStateConnecting(false,mConnectionState);
//                return false;
//            }
//        }
//
        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            setStateConnecting(false,mConnectionState);
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);

        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        mConnectionState = BongState.STATE_CONNECTING.getTypeValue();
        setStateConnecting(true,mConnectionState);
        return true;
    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                String intentAction;
                Log.d(TAG, "Current state:" + newState);
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    intentAction = ACTION_GATT_CONNECTED;
                    mConnectionState = BongState.STATE_CONNECTED.getTypeValue();
                    setStateConnecting(true,mConnectionState);
                    broadcastUpdate(intentAction);
                    Log.i(TAG, "Connected to GATT server.");
                    // Attempts to discover services after successful connection.
                    Log.i(TAG, "Attempting to start service discovery:" +
                            mBluetoothGatt.discoverServices());

                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    intentAction = ACTION_GATT_DISCONNECTED;
                    mConnectionState = BongState.STATE_DISCONNECTED.getTypeValue();
                    setStateConnecting(false,mConnectionState);
                    Log.i(TAG, "Disconnected from GATT server.");
                    broadcastUpdate(intentAction);
                }
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                } else {
                    Log.w(TAG, "onServicesDiscovered received: " + status);
                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic,
                                             int status) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt,BluetoothGattCharacteristic characteristic) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        };

    private void broadcastUpdate(final String action) {
            final Intent intent = new Intent(action);
//            System.out.println("Sending int")
        Log.d("BOngs", "Sending intent:" + action);
        sendBroadcast(intent);
        }

    private void broadcastUpdate(final String action,final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);
        // This is special handling for the Heart Rate Measurement profile.  Data parsing is
        // carried out as per profile specifications:
        // http://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml
      if (characteristic.getUuid().toString().equals(UIDevice.RX_CHARACTERISTIC.getTypeValue())) {
//          Log.d(TAG, "Heart rate format HEART_RATE_MEASUREMENT.");
          int flag = characteristic.getProperties();
          int format = -1;
          String data = null;
          if ((flag & 0x01) != 0) {
              format = BluetoothGattCharacteristic.FORMAT_UINT16;
              Log.d(TAG, "Heart rate format UINT16.");
          } else {
              format = BluetoothGattCharacteristic.FORMAT_UINT8;
              Log.d(TAG, "Heart rate format UINT8.");
          }

          final int statusChannel = characteristic.getIntValue(format,1);
          final int heartRateResult = characteristic.getIntValue(format,2);
          if (statusChannel==1){
              if (heartRateResult==0){
                  Log.d(TAG, String.format("In measure,not ready."));
                  intent.putExtra(EXTRA_DATA, String.valueOf("In measure,not ready."));
                  data=String.valueOf("In measure,not ready.");
              }else if (heartRateResult==1){
                  final int heartRateValue = characteristic.getIntValue(format,3);
                  Log.d(TAG, String.format("Received heart rate: %d", heartRateValue));
                  intent.putExtra(EXTRA_DATA,String.format("Received heart rate: %d", heartRateValue));
                  data=String.format("Received heart rate: %d", heartRateValue);
              }
              setOnReceivedData(OperationType.HEARTRATE,data);

          }else if (statusChannel==2){
              if (heartRateResult==0){
                  final int heartRateValue = characteristic.getIntValue(format,3);
                  Log.d(TAG, String.format("In charging battery: %d", heartRateValue));
                  intent.putExtra(EXTRA_DATA, String.format("In charging battery: %d", heartRateValue));
                  data=String.format("In charging battery: %d", heartRateValue);
              }else if(heartRateResult==1){
                  final int heartRateValue = characteristic.getIntValue(format,3);
                  Log.d(TAG, String.format("In charging battery: %d", heartRateValue));
                  intent.putExtra(EXTRA_DATA, String.format("Out of charging battery: %d", heartRateValue));
                  data=String.format("Out of charging battery: %d", heartRateValue);
              }
              setOnReceivedData(OperationType.BATTERY,data);

          }
          ///eles if ของ STEPS ยังไม่ถูกเขียน


      }//else {
        // For all other profiles, writes the data formatted in HEX.
        final byte[] data = characteristic.getValue();
        if (data != null && data.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(data.length);
            for(byte byteChar : data)
                stringBuilder.append(String.format("%02X ", byteChar));
//            intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            Log.i(TAG, "ข้อมูล Service HEX. ::"+new String(data)+" <-> "+stringBuilder.toString());

        }
     // }
        sendBroadcast(intent);
    }

    private final BluetoothGattServerCallback mGattServerCallback = new BluetoothGattServerCallback() {
        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            Log.d("HELLO", "Our gatt server connection state changed, new state ");
            Log.d("HELLO", Integer.toString(newState));
            super.onConnectionStateChange(device, status, newState);
        }

        @Override
        public void onServiceAdded(int status, BluetoothGattService service) {
            Log.d("HELLO", "Our gatt server service was added.");
            super.onServiceAdded(status, service);
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
            Log.d("HELLO", "Our gatt characteristic was read.");
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId, BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
            Log.d("HELLO", "We have received a write request for one of our hosted characteristics");

            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value);
        }

        @Override
        public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattDescriptor descriptor) {
            Log.d("HELLO", "Our gatt server descriptor was read.");
            super.onDescriptorReadRequest(device, requestId, offset, descriptor);
        }

        @Override
        public void onDescriptorWriteRequest(BluetoothDevice device, int requestId, BluetoothGattDescriptor descriptor, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
            Log.d("HELLO", "Our gatt server descriptor was written.");
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
        }

        @Override
        public void onExecuteWrite(BluetoothDevice device, int requestId, boolean execute) {
            Log.d("HELLO", "Our gatt server on execute write.");
            super.onExecuteWrite(device, requestId, execute);
        }
    };

    @Override
    public boolean onUnbind(Intent intent) {
            close();
            return super.onUnbind(intent);
        }

    private final IBinder mBinder = new LocalBinder();

    public boolean initialize() {
            // For API level 18 and above, get a reference to BluetoothAdapter through
            // BluetoothManager.
            if (mBluetoothManager == null) {
                mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                if (mBluetoothManager == null) {
                    setMessageError( "Unable to initialize BluetoothManager.");
                    Log.e(TAG, "Unable to initialize BluetoothManager.");

                    return false;
                }
            }

            mBluetoothAdapter = mBluetoothManager.getAdapter();
            if (mBluetoothAdapter == null) {
                setMessageError("Unable to obtain a BluetoothAdapter.");
                Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
                return false;
            }

            //JDD - lets go ahead and start our GATT server now
            addDefinedGattServerServices();

            return true;
        }

    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
            if (mBluetoothAdapter == null || mBluetoothGatt == null) {
                Log.w(TAG, "BluetoothAdapter not initialized");
                return;
            }
            mBluetoothGatt.readCharacteristic(characteristic);
    }
    @Override
    public void writeRemoteCharacteristic(BluetoothGattCharacteristic characteristic) {

            if (mBluetoothAdapter == null || mBluetoothGatt == null) {
                Log.w(TAG, "BluetoothAdapter not initialized");
                return;
            }
        if (mBluetoothGatt.writeCharacteristic(characteristic)) {
            Log.d(TAG, "In write(BluetoothGattCharacteristic characteristic):: ได้");
        }else Log.d(TAG,"In write(BluetoothGattCharacteristic characteristic):: ไม่ได้");
    }

    @Override
    public void writeRemoteCharacteristic(BluetoothGattCharacteristic characteristic, byte[] value) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        characteristic.setValue(value);
        if (mBluetoothGatt.writeCharacteristic(characteristic)) {
            Log.d(TAG, "In write(BluetoothGattCharacteristic characteristic, byte[] value):: ได้");
        }else Log.d(TAG,"In write(BluetoothGattCharacteristic characteristic, byte[] value):: ไม่ได้");
    }

    @Override
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,boolean enabled) {
            if (mBluetoothAdapter == null || mBluetoothGatt == null) {
                Log.w(TAG, "BluetoothAdapter not initialized");
                return;
            }
            mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        if (characteristic.getUuid().toString().equals(UIDevice.RX_CHARACTERISTIC.getTypeValue())) {
            Log.d(TAG,"In Loob Data!!");
            UUID uuid = UUID.fromString(UIDevice.CLIENT_CHARACTERISTIC_CONFIG.getTypeValue());
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(uuid);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(descriptor);
        }
    }

    @Override
    public List<BluetoothGattService> getSupportedGattServices() {
            if (mBluetoothGatt == null) return null;
            return mBluetoothGatt.getServices();
        }

    public void addGattServerService(BluetoothGattService service){
            mBluetoothGattServer.addService(service);
        }

    public void addDefinedGattServerServices(){
            Log.d("HELLO", "Created our own GATT server.\r\n");

    }


    @Override
    public void writeRemoteCharacteristic(byte[] value){

        //check mBluetoothGatt is available
        if (mBluetoothGatt == null) {
            setMessageError("lost connection");
            Log.e(TAG, "lost connection");
        }
        BluetoothGattService Service = mBluetoothGatt.getService(UUID.fromString(UIDevice.SERVICE.getTypeValue()));
        if (Service == null) {
            setMessageError("service not found!");
            Log.e(TAG, "service not found!");
        }
        BluetoothGattCharacteristic charac = Service.getCharacteristic(UUID.fromString(UIDevice.TX_CHARACTERISTIC.getTypeValue()));
//        charac.setValue(value);
        if (charac == null) {
            setMessageError("char not found!");
            Log.e(TAG, "char not found!");
        }

        if (mBluetoothGatt.writeCharacteristic(charac)) {
            Log.d(TAG, "In write(byte[] value):: ได้");
        }else Log.d(TAG,"In write(byte[] value):: ไม่ได้");
    }

    @Override
    public void setCharacteristicNotification(boolean enabled){
        if (mBluetoothGatt == null) {
            setMessageError("lost connection");
            Log.e(TAG, "lost connection");
        }
        BluetoothGattService Service = mBluetoothGatt.getService(UUID.fromString(UIDevice.SERVICE.getTypeValue()));
        if (Service == null) {
            setMessageError("service not found!");
            Log.e(TAG, "service not found!");
        }
        BluetoothGattCharacteristic charac = Service.getCharacteristic(UUID.fromString(UIDevice.RX_CHARACTERISTIC.getTypeValue()));
        if (charac == null) {
            setMessageError("char not found!");
            Log.e(TAG, "char not found!");
        }
        setCharacteristicNotification(charac,enabled);
    }

}