package com.ost.wearable;

/**
 * Created by Nattanon-PC on 15/7/2016.
 */
public enum OperationType {
    STEPS(2),
    HEARTRATE(1),
    BATTERY(3);
    private byte[] typeValue;

    OperationType(int typeValue) {
        switch(typeValue){
            case 1:
                this.typeValue = new byte[]{0x26, 0x00, 0x00, 0x00, 0x52};
                break;
            case 2:
                this.typeValue = new byte[]{0x20,0x00,0x00,0x00,0x13,0x10,0x07,0x0C,0x10,0x00,0x10,0x07,0x0C,0x10,0x10};
                break;
            default:
                this.typeValue = null;
                break;
        }
    }
    public byte[] getTypeValue() {
        return this.typeValue;
    }
}
